DROP DATABASE trackit_db;
CREATE DATABASE trackit_db;

DROP TABLE trackit_users;
CREATE TABLE trackit_users
(
    user_id   INTEGER      NOT NULL AUTO_INCREMENT,
    username  VARCHAR(30)  NOT NULL,
    password  VARCHAR(30)  NOT NULL,
    firstname VARCHAR(30)  NOT NULL,
    lastname  VARCHAR(30)  NOT NULL,
    email     VARCHAR(100) NOT NULL,
    PRIMARY KEY (user_id)
);

DROP TABLE trackit_projects;
CREATE TABLE trackit_projects
(
    project_id      INTEGER     NOT NULL AUTO_INCREMENT,
    projectname     VARCHAR(20) NOT NULL,
    projectcode     VARCHAR(5)  NOT NULL,
    private         SMALLINT    NOT NULL,
    flags           INTEGER,
    PRIMARY KEY (project_id)
);

DROP TABLE trackit_tickets;
CREATE TABLE trackit_tickets
(
    ticket_id       INTEGER     NOT NULL AUTO_INCREMENT,
    project_id      INTEGER     NOT NULL,
    subject         VARCHAR(20) NOT NULL,
    description     TEXT,
    meta_tags       VARCHAR,
    flags           INTEGER,
    assigneduser    INTEGER     NOT NULL,
    PRIMARY KEY (ticket_id),
    FOREIGN KEY (project_id) 
        REFERENCES trackit_projects(project_id) 
        ON DELETE CASCADE
);

DROP TABLE trackit_user_project;
CREATE TABLE trackit_user_project
(
    user_project_id INTEGER     NOT NULL AUTO_INCREMENT,
    user_id         INTEGER     NOT NULL,
    project_id      INTEGER     NOT NULL,
    PRIMARY KEY (ticket_project_id),
    FOREIGN KEY (project_id) 
        REFERENCES trackit_projects(project_id),
    FOREIGN KEY (ticket_id) 
        REFERENCES trackit_users(user_id) 
);